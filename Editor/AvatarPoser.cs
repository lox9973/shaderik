﻿using System.Collections.Generic;
using System.Linq;
using Array = System.Array;
using Path = System.IO.Path;
using UnityEngine;
using UnityEditor;
using UnityEngine.Animations;
namespace ShaderIK {
public class AvatarPoser {
	public static void ResetPose(Animator animator, bool tpose=false, bool resetScale=false) {
		var avatarSO = new SerializedObject(animator.avatar);
		
		var tos = avatarSO.FindProperty("m_TOS");
		var idToPath = new Dictionary<uint, string>();
		for(int i=0; i<tos.arraySize; i++) {
			var elem = tos.GetArrayElementAtIndex(i);
			var id   = (uint)elem.FindPropertyRelative("first").intValue;
			var path = elem.FindPropertyRelative("second").stringValue;
			idToPath.Add(id, path);
		}

		var pose = avatarSO.FindProperty(tpose ? "m_Avatar.m_AvatarSkeletonPose.data.m_X" : "m_Avatar.m_DefaultPose.data.m_X");
		var nameid = avatarSO.FindProperty("m_Avatar.m_AvatarSkeleton.data.m_ID");
		for(int i=0; i<nameid.arraySize; i++) {
			var id = (uint)nameid.GetArrayElementAtIndex(i).intValue;
			var path = idToPath[id];

			var tqs = pose.GetArrayElementAtIndex(i);
			var position = new Vector3(
				tqs.FindPropertyRelative("t.x").floatValue,
				tqs.FindPropertyRelative("t.y").floatValue,
				tqs.FindPropertyRelative("t.z").floatValue);
			var rotation = new Quaternion(
				tqs.FindPropertyRelative("q.x").floatValue,
				tqs.FindPropertyRelative("q.y").floatValue,
				tqs.FindPropertyRelative("q.z").floatValue,
				tqs.FindPropertyRelative("q.w").floatValue);
			var scale = new Vector3(
				tqs.FindPropertyRelative("s.x").floatValue,
				tqs.FindPropertyRelative("s.y").floatValue,
				tqs.FindPropertyRelative("s.z").floatValue);

			var t = animator.transform.Find(path);
			if(t != null && path != "") {
				t.localPosition = position;
				t.localRotation = rotation;
				if(resetScale)
					t.localScale = scale;
			}
		}
	}
	public static void SetSkeletonPose(Animator animator) {
		ResetPose(animator, true);
	}
	public static void SetMuscle(Animator animator, Vector3[] values) {
		var hips = animator.GetBoneTransform(HumanBodyBones.Hips);
		var position = hips.position;
		var handler = new HumanPoseHandler(animator.avatar, animator.transform);
		var pose = new HumanPose();
		handler.GetHumanPose(ref pose);

		var muscles = new float[HumanTrait.MuscleCount];
		for(int bone=0; bone<HumanTrait.BoneCount; bone++)
			for(int axis=0; axis<3; axis++) {
				var muscle = HumanTrait.MuscleFromBone(bone, axis);
				if(muscle >= 0)
					muscles[muscle] = values[bone][axis];
			}

		pose.muscles = muscles;
		handler.SetHumanPose(ref pose);
		hips.position = position;
	}
	public static void RelaxMuscle(Animator animator) {
		var muscles = new Vector3[HumanTrait.BoneCount];
		muscles[(int)HumanBodyBones.LeftLowerArm ].x = 0.5f;
		muscles[(int)HumanBodyBones.RightLowerArm].x = 0.5f;
		AvatarPoser.SetMuscle(animator, muscles);
	}
	[MenuItem("ShaderIK/T-Pose Avatar")]
	static void SetSkeletonPose() {
		var animator = Selection.activeGameObject.GetComponentInParent<Animator>();
		Debug.Assert(animator && animator.isHuman, "expect a humanoid animator");
		SetSkeletonPose(animator);
	}
	[MenuItem("ShaderIK/Relax Muscle")]
	static void RelaxMuscle() {
		var animator = Selection.activeGameObject.GetComponentInParent<Animator>();
		Debug.Assert(animator && animator.isHuman, "expect a humanoid animator");
		RelaxMuscle(animator);
	}
}
}