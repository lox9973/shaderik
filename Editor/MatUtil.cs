using System.Collections.Generic;
using System.Linq;
using Array = System.Array;
using UnityEngine;
using UnityEditor;
namespace ShaderIK {
public class MatUtil {
	public static void RemoveUnusedProperties(Material mat) {
		var so = new SerializedObject(mat);
		var propsArr = new[]{
			so.FindProperty("m_SavedProperties.m_TexEnvs"),
			so.FindProperty("m_SavedProperties.m_Floats"),
			so.FindProperty("m_SavedProperties.m_Colors"),
		};
		foreach(var props in propsArr)
			for(int i = props.arraySize - 1; i >= 0; i--) {
				var name = props.GetArrayElementAtIndex(i).FindPropertyRelative("first").stringValue;
				if(!mat.HasProperty(name)) {
					Debug.Log($"remove property {name}");
					props.DeleteArrayElementAtIndex(i);
				}
			}
		so.ApplyModifiedProperties();
	}
	public static void ResetVectorProperties(Material mat) {
		var names = new HashSet<string>();
		for(int i = ShaderUtil.GetPropertyCount(mat.shader)-1; i >= 0; i--)
			if(ShaderUtil.GetPropertyType(mat.shader, i) == ShaderUtil.ShaderPropertyType.Vector)
				names.Add(ShaderUtil.GetPropertyName(mat.shader, i));

		var so = new SerializedObject(mat);
		var props = so.FindProperty("m_SavedProperties.m_Colors");
		for(int i = props.arraySize - 1; i >= 0; i--) {
			var name = props.GetArrayElementAtIndex(i).FindPropertyRelative("first").stringValue;
			if(names.Contains(name)) {
				// Debug.Log($"remove property {name}");
				props.DeleteArrayElementAtIndex(i);
			}
		}
		so.ApplyModifiedProperties();
	}
	[MenuItem("CONTEXT/Material/RemoveUnusedProperties")]
	static void RemoveUnusedProperties(MenuCommand command) {
		var mat = (Material) command.context;
		RemoveUnusedProperties(mat);
	}
	[MenuItem("CONTEXT/Material/ResetVectorProperties")]
	static void ResetVectorProperties(MenuCommand command) {
		var mat = (Material) command.context;
		ResetVectorProperties(mat);
	}
}
}